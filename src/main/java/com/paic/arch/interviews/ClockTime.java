package com.paic.arch.interviews;

import org.apache.commons.lang.Validate;

import static org.apache.commons.lang.math.NumberUtils.toInt;

/**
 * 时钟时间实体
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/27
 */

public class ClockTime {

    private int hours;
    private int minutes;
    private int seconds;

    public ClockTime(String aTime) {
        Validate.notEmpty(aTime, "create ClockTime error, aTime should not be null");
        String[] timeInfos = aTime.split(":");
        if (timeInfos.length!=3) {
            throw new IllegalArgumentException("create ClockTime error, aTime format is not up to standard.");
        }
        this.hours = toInt(timeInfos[0]);
        this.minutes = toInt(timeInfos[1]);
        this.seconds = toInt(timeInfos[2]);
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
