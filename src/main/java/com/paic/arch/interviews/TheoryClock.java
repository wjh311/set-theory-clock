package com.paic.arch.interviews;

import org.apache.commons.lang.StringUtils;

/**
 * 时钟实体
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/27
 */

public class TheoryClock {

    private String secondLight;
    private String fiveHourLights;
    private String oneHourLights;
    private String fiveMinuteLights;
    private String oneMinuteLights;

    public TheoryClock(ClockTime clockTime) {
        this.secondLight = formatSecondLight(clockTime);
        this.fiveHourLights = formatFiveHourLights(clockTime);
        this.oneHourLights = formatOneHourLights(clockTime);
        this.fiveMinuteLights = formatFiveMinuteLights(clockTime);
        this.oneMinuteLights = formatOneMinuteLights(clockTime);
    }

    /**
     * 秒标志灯格式化，规则：两秒钟闪烁一次的黄灯
     *
     * @param clockTime 输入时间
     * @return 秒标志灯格式化字符串
     */
    private String formatSecondLight(ClockTime clockTime) {
        ClockLightState light = clockTime.getSeconds() % 2 == 0 ? ClockLightState.YELLOW : ClockLightState.CLOSE;
        return light.toString();
    }

    /**
     * 5小时标志灯格式化，规则：每5小时点亮一盏红灯，共4盏
     *
     * @param clockTime 输入时间
     * @return 5小时标志灯格式化字符串
     */
    private String formatFiveHourLights(ClockTime clockTime) {
        return formatLightStr(ClockLightState.RED, clockTime.getHours() / 5, 4);
    }

    /**
     * 1小时标志灯格式化，规则：每1小时点亮一盏红灯，共4盏
     *
     * @param clockTime 输入时间
     * @return 1小时标志灯格式化字符串
     */
    private String formatOneHourLights(ClockTime clockTime) {
        return formatLightStr(ClockLightState.RED, clockTime.getHours() % 5, 4);
    }

    /**
     * 5分钟标志灯格式化，规则：每5分钟点亮一盏灯，共11盏，第3、6、9盏灯为红色，其余为黄色
     *
     * @param clockTime 输入时间
     * @return 5分钟标志灯格式化字符串
     */
    private String formatFiveMinuteLights(ClockTime clockTime) {
        ClockLightState[] lights = formatLight(ClockLightState.YELLOW, clockTime.getMinutes() / 5, 11);
        if (lights == null) {
            return "";
        }
        changeLightIfExist(lights, 2, ClockLightState.YELLOW, ClockLightState.RED);
        changeLightIfExist(lights, 5, ClockLightState.YELLOW, ClockLightState.RED);
        changeLightIfExist(lights, 8, ClockLightState.YELLOW, ClockLightState.RED);
        return convertLight2Str(lights);
    }

    /**
     * 1分钟标志灯格式化，规则：每1分钟点亮一盏黄灯，共4盏
     *
     * @param clockTime 输入时间
     * @return 1分钟标志灯格式化字符串
     */
    private String formatOneMinuteLights(ClockTime clockTime) {
        return formatLightStr(ClockLightState.YELLOW, clockTime.getMinutes() % 5, 4);
    }

    private String formatLightStr(ClockLightState light, int times, int length) {
        return convertLight2Str(formatLight(light, times, length));
    }

    /**
     * 将灯组格式化成字符串输出
     *
     * @param lights 灯组
     * @return 灯组格式化字符串
     */
    private String convertLight2Str(ClockLightState... lights) {
        return lights == null ? "" : StringUtils.join(lights);
    }

    private ClockLightState[] formatLight(ClockLightState light, int times, int length) {
        return formatLight(light, times, length, ClockLightState.CLOSE);
    }

    /**
     * 构造灯组
     *
     * @param light     灯类型
     * @param times     灯个数
     * @param length    灯组总长度
     * @param fillLight 长度不足，填充灯类型
     * @return 灯组
     */
    private ClockLightState[] formatLight(ClockLightState light, int times, int length, ClockLightState fillLight) {
        if (length <= 0) {
            return null;
        }
        ClockLightState[] lights = new ClockLightState[length];
        for (int i = 0; i < length; i++) {
            lights[i] = i < times ? light : fillLight;
        }
        return lights;
    }

    /**
     * 替换灯组指定位置指定类型的灯为其他类型
     *
     * @param lights      灯组
     * @param index       指定灯序号
     * @param checkLight  指定灯类型
     * @param changeLight 替换灯
     */
    private void changeLightIfExist(ClockLightState[] lights, int index, ClockLightState checkLight, ClockLightState changeLight) {
        if (lights != null && lights[index] == checkLight) {
            lights[index] = changeLight;
        }
    }

    @Override
    public String toString() {
        return StringUtils.join(new String[]{secondLight, fiveHourLights, oneHourLights, fiveMinuteLights, oneMinuteLights}, System.lineSeparator());
    }
}
