package com.paic.arch.interviews;

/**
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/27
 */

public class TimeConverterImpl implements TimeConverter {

    @Override
    public String convertTime(String aTime) {
        // 创建时钟时间对象
        ClockTime clockTime = new ClockTime(aTime);
        // 创建时间对象
        TheoryClock clock = new TheoryClock(clockTime);
        return clock.toString();
    }
}
