package com.paic.arch.interviews;

/**
 * 时钟灯状态
 *
 * @author 吴建宏
 * @version v1.0
 * @date 2018/2/27
 */

public enum ClockLightState {
    CLOSE("O"),
    RED("R"),
    YELLOW("Y");

    private String lightFlag;
    ClockLightState(String lightFlag){
        this.lightFlag = lightFlag;
    }

    @Override
    public String toString() {
        return lightFlag;
    }
}
